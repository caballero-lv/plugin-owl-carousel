
	<?php
	$img = get_field( 'owl-image' );
	$text_1 = get_field( 'owl-text-1' );
	$text_2 = get_field( 'owl-text-2' );
	$l_url = get_field( 'owl-link-url' );
	?>
	<?php $img = wp_get_attachment_image_src( $img, 'slider', null, array( 'class' => 'owl-slide-img' ) ); ?>
	<div class="slide-img" style="background-image:url(<?php echo $img[0]; ?>)">
	<!-- <img src="<?php echo $img[0]; ?>" class="slide-img" /> -->
	<div class="owl-slide-content">
		<div class="owl-slide-inner container animate-400">
			<div class="text-wrap">
			<div class="text-wrap-inner">
				<?php if( strlen( $l_url ) ): ?>
					<a href="<?php echo add_http($l_url); ?>">
				<?php endif; ?>
					<?php if(strlen($text_1)): ?>
						<h1 class="owl-slide-title"><?php echo $text_1; ?></h1>
					<?php endif; ?>
					<?php if(strlen($text_2)): ?>
						<div class="owl-slide-title">
							<div class="table">
								<div class="table-cell"><?php echo $text_2; ?></div>
							</div>
						</div>
					<?php endif; ?>

				<?php if( strlen( $l_url ) ): ?>
					</a>
				<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>