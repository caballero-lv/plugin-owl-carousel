<?php

	/*
	Plugin Name: DF Owl Carousel
	Author: Eriks Briedis
	Version: 0.1
	Author URI: http://www.datafix.lv/
	*/


	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}

	class DF_Owl_Carousel {

		public $owl_dir;
		public $owl_url;

		function __construct(){

			$this->owl_dir = plugin_dir_path( __FILE__ );
			$this->owl_url = plugins_url( '/', __FILE__ );

			add_action('wp_enqueue_scripts', array( $this, 'owl_enqueue_scripts' ));

			add_action('admin_menu', array( $this, 'add_owl_settings' ));
			$this->add_owl_post_type();
			$this->add_owl_custom_fields();
		}

		public function owl_enqueue_scripts(){

			wp_enqueue_script( 'jquery' );
			wp_enqueue_style( 'owl-style', $this->owl_url . '/assets/css/owl.carousel.css' );
			wp_enqueue_script( 'owl-carousel', $this->owl_url . '/assets/js/owl.carousel.min.js', array('jquery'), '1.3.3', true );
			wp_enqueue_script( 'df-owl-scripts', $this->owl_url . '/assets/js/df-owl-scripts.js', array( 'jquery', 'owl-carousel' ), '0.1', true );
		}

		public function add_owl_settings(){}

		public function add_owl_post_type(){

			$this->load( 'Slide' );
		}

		public function add_owl_custom_fields(){
			if(function_exists("register_field_group"))
			{
				register_field_group(array (
					'id' => 'acf_slide',
					'title' => 'Slide',
					'fields' => array (
						array (
							'key' => 'field_537afe60dfc12',
							'label' => 'Image',
							'name' => 'owl-image',
							'type' => 'image',
							'save_format' => 'id',
							'preview_size' => 'medium',
							'library' => 'all',
						),
						
						array (
							'key' => 'field_53bfdbf7d432d',
							'label' => 'Text 1',
							'name' => 'owl-text-1',
							'type' => 'text',
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'formatting' => 'html',
							'maxlength' => '',
						),
						array (
							'key' => 'field_53bfdc19d432e',
							'label' => 'Text 2',
							'name' => 'owl-text-2',
							'type' => 'text',
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'formatting' => 'html',
							'maxlength' => '',
						),
						array (
							'key' => 'field_537afe94dfc15',
							'label' => 'Link URL',
							'name' => 'owl-link-url',
							'type' => 'text',
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'formatting' => 'html',
							'maxlength' => '',
						),
					),
					'location' => array (
						array (
							array (
								'param' => 'post_type',
								'operator' => '==',
								'value' => 'slide',
								'order_no' => 0,
								'group_no' => 0,
							),
						),
					),
					'options' => array (
						'position' => 'normal',
						'layout' => 'default',
						'hide_on_screen' => array (
						),
					),
					'menu_order' => 0,
				));
			}

		}


		public function render(){

			$args = array(
				'post_type' 	 => 'slide',
				'posts_per_page' => -1
				);
			$query = new Wp_Query( $args );

			if( $query->have_posts() ) :
				echo '<div class="df-owl-wrap">';
					while( $query->have_posts() ) : $query->the_post();

						include $this->owl_dir . '/html/slide.php';

					endwhile;
				echo '</div>';
			endif;
			wp_reset_query();
		}

		protected function load( $class = false ){

			if( $class === false ){
				return false;
			}

			require_once $this->owl_dir . 'classes/' . $class . '.php';
		}
	}

	$df_owl_carousel = new DF_Owl_Carousel();

	function put_owl_slider(){
		$df_owl_carousel = new DF_Owl_Carousel();
		$df_owl_carousel->render();
	}