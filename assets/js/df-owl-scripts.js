(function ($) {

    if ($('.df-owl-wrap').length) {

        var slider_height = 440;
        var time = 7; // time in seconds

        var $progressBar,
            $bar,
            $elem,
            isPause,
            tick,
            percentTime;

        var slider = $('.df-owl-wrap'),
            thumbs = $('.df-owl-thumbs');




        $('body').on('click', '.owl-left', function (e) {
            e.preventDefault();
            slider.trigger('owl.prev');
        }).on('click', '.owl-right', function (e) {
            e.preventDefault();
            slider.trigger('owl.next');
        });


        //Init the carousel
        slider.owlCarousel({
            slideSpeed: 500,
            paginationSpeed: 500,
            singleItem: true,
            responsiveRefreshRate : 50,
            transitionStyle: "fade",
            // autoHeight: true,
            pagination: true,
            mouseDrag: false,
            lazyLoad: false,
            autoPlay : true,
            afterMove: function(){

                $('.owl-item').removeClass('current-owl');
                $('.owl-item').eq( this.currentItem ).addClass('current-owl');
            },
            afterInit: function(el){
                if ($('.owl-item').length == 1) {
                   slider.trigger('owl.stop');
                }
                $('.owl-wrapper').children('.owl-item').eq(0).addClass('current-owl');
                $('.owl-wrapper').imagesLoaded(function(){
                    $('.df-owl-wrap').css({
                        visibility:'visible',
                    });
                });
                
            }
        });
    }
})(jQuery);