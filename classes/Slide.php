<?php

	// register a custom post type for Slide

	class Slide{

		function __construct(){

			add_action( 'init', array( $this, 'init_owl_slide' ) ); // post type
			add_filter( 'manage_posts_columns', array( $this, 'owl_add_img_column') );
			add_action( 'manage_posts_custom_column', array( $this, 'owl_manage_img_column') );

			//add_

			//add_action( 'init', array( $this, 'init_product_tax' ) ); // taxonomy
		}

		// custom post type register
		public function init_owl_slide(){

			register_post_type( 'slide',
				array(
					'labels' => array(
						'name' => __( 'Slider' ),
						'singular_name' => __( 'Slide' )
					),
				'public' => true,
				'has_archive' => true,
				'menu_position' => 24,
				'supports' => array(
					'title',
				)
				)
			);
		}


		public function owl_add_img_column( $columns ) {

			if( get_post_type() == 'slide' ){
				$columns['owl_img'] = 'Thumbnail';
			}

			return $columns;
		}

		public function owl_manage_img_column( $column ){
			global $post;
			switch ($column) {
				case 'owl_img':
					$img = get_field( 'owl-image', $post->ID );
					echo wp_get_attachment_image( $img, 'medium' );
					break;
			}
		}

		public function init_product_tax(){

			register_taxonomy(
				'product-categories',
				'product',
				array(
					'label' => __( 'Product categories' ),
					'hierarchical' 			=> true,
					'show_admin_column' => true,
					'rewrite' => empty( $permalinks['tag_base'] ) ? 'product-category' : $permalinks['tag_base']
				)
			);

			register_taxonomy(
				'featured-products',
				'product',
				array(
					'label' => __( 'Featured products' ),
					'hierarchical' 			=> true,
					'show_admin_column' => true,
					'rewrite' => empty( $permalinks['tag_base'] ) ? 'featured-products' : $permalinks['tag_base']
				)
			);
		}
	}

	new Slide();